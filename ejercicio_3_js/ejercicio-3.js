var dniNumber = window.prompt("Introduce los 8 numeros del DNI (Sin la letra)");

if (dniNumber > 0 && dniNumber <= 99999999 && dniNumber.length == 8) {
    var dniLetter = window.prompt("Introduce ahora la letra del DNI");
    var dniLetterUppercase = dniLetter.toUpperCase();
    var numberLetter = dniNumber % 23;
    var letters = [
        "T",
        "R",
        "W",
        "A",
        "G",
        "M",
        "Y",
        "F",
        "P",
        "D",
        "X",
        "B",
        "N",
        "J",
        "Z",
        "S",
        "Q",
        "V",
        "H",
        "L",
        "C",
        "K",
        "E",
    ];

    for (let i = 0; i < letters.length; i++) {
        if (numberLetter == letters.indexOf(letters[i]) && dniLetterUppercase == letters[i]) {
            window.alert("El DNI es Válido");
        } else if (
            numberLetter == letters.indexOf(letters[i]) &&
            dniLetterUppercase != letters[i]
        ) {
            window.alert("El número de DNI no es válido");
        }
    }
} else {
    window.alert("El número de DNI no es válido");
}
